# allersist

- for building on iOS

1. edit `Allersist/ios/Allersist/Info.plist`
  and add:
  
```
  	<key>UIAppFonts</key>
	<array>
		<string>Ionicons.ttf</string>
	</array>
```

2. run from project root:

```
cp node_modules/react-native-vector-icons/Fonts/Ionicons.ttf ios/Fonts/Ionicons.ttf
```


