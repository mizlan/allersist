/* not currently in use */

import React from 'react';
import {View, Text, StyleSheet, TouchableHighlight} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import WebsiteArea from './WebsiteArea';

const SettingsWidget = ({item}) => {
  const restaurant = item['restaurant'];
  const address = restaurant['address'];
  const url = restaurant['website'];
  return (
    <View style={styles.container}>
      <View
        style={{
          flex: 1,
          flexDirection: 'row',
          justifyContent: 'space-between',
        }}>
        <Text style={styles.restaurantName}>{restaurant['description']}</Text>
        <Text>
          {item['sortScore'].toFixed(2)}{' '}
          <Ionicons name="star" style={{color: '#e6c42e'}} size={16} />
        </Text>
      </View>
      <Text>{item['distance'].toFixed(2)} mi</Text>
      <Text>
        <Ionicons name="location" style={{color: '#a4a6a5'}} />{' '}
        {address['line1'] + ', ' + address['city']}
      </Text>
      <View
        style={styles.website}>
        <WebsiteArea url={url} description="go to website" />
        <WebsiteArea
          url={`https://www.yelp.com/search?find_desc=${restaurant['description']}`}
          description="search on Yelp"
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    padding: 15,
  },
  restaurantName: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  website: {
    flex: 1,
    flexDirection: 'row',
  }
});

export default SettingsWidget;
