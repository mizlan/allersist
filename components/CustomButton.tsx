import React from 'react';
import {StyleSheet, TouchableHighlight} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';

interface CustomButtonProps {
  onPress: any;
  style: any;
  underlayColor: string;
}

const CustomButton = ({
  onPress,
  style,
  underlayColor,
  ...props
}: CustomButtonProps) => {
  return (
    <TouchableHighlight
      style={[styles.buttonDefault, style]}
      onPress={onPress}
      underlayColor={underlayColor}>
      {props.children}
    </TouchableHighlight>
  );
};

const styles = StyleSheet.create({
  buttonDefault: {
    padding: 8,
    margin: 5,
    backgroundColor: '#d6d4d5',
    borderRadius: 5,
    alignItems: 'center',
  },
});

export default CustomButton;
