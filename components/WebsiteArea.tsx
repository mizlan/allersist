import React from 'react';
import {View, Text, StyleSheet, TouchableHighlight} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';

import handleOpenURL from '../util/handleOpenURL';

interface WebsiteAreaProps {
  url: string | null | undefined;
  description: string;
}

const WebsiteArea = ({url, description}: WebsiteAreaProps) => {
  if (url === null || url === undefined || url.trim() === '') {
    return (
      <View style={styles.dull}>
        <Text style={styles.noWebsiteText}>no website provided</Text>
      </View>
    );
  } else {
    return (
      <TouchableHighlight
        onPress={() => handleOpenURL(url)}
        underlayColor="#676767"
        style={styles.website}>
        <Text style={styles.websiteText}>{description}</Text>
      </TouchableHighlight>
    );
  }
};

const styles = StyleSheet.create({
  dull: {
    backgroundColor: '#e1e5d8',
    padding: 8,
    marginRight: 5,
    marginTop: 5,
  },
  website: {
    backgroundColor: '#e1e5d8',
    padding: 8,
    marginRight: 5,
    marginTop: 5,
  },
  noWebsiteText: {
    fontStyle: 'italic',
  },
  websiteText: {
    color: 'blue',
  },
});

export default WebsiteArea;
