/*
 * the Getting Started menu, for adding new OIT configs
 */

import React, {useState, useReducer} from 'react';
import {View, Text, StyleSheet, TextInput} from 'react-native';

import AsyncStorage from '@react-native-async-storage/async-storage';
import CustomButton from './CustomButton';

// TODO: make this one item
const saveOITData = async (ITConfig) => {
  asyncKey = '@immunotherapy_ITJSON';
  serialized = JSON.stringify(ITConfig);
  configKey = serialized['ITName'];
  allITConfigs = [];
  storedConfigs = await AsyncStorage.getItem(asyncKey);
  if (storedConfigs !== null) {
    console.log('found stored Configs', JSON.stringify(storedConfigs));
    allITConfigs = JSON.parse(storedConfigs);
    for (let config of allITConfigs) {
      if (config['ITName'] == configKey) {
        return 1;
      }
    }
  }
  console.log(allITConfigs);
  allITConfigs.push(ITConfig);
  AsyncStorage.setItem(asyncKey, JSON.stringify(allITConfigs));
};

// There’s a couple key rules for reducers. Reducers: Never mutate their
// arguments (no modification of the currentState) Never generate side affects
// (no external functional calls or API requests) Never call non pure functions
// that change their output based on anything other than their input (i.e. never
// use Date.now() or any changing value)

const ITGetStarted = ({guideName}) => {
  const [ITName, setITName] = useState('Peanuts');
  const [curDosage, setCurDosage] = useState('0');
  const [updoseAmt, setUpdoseAmt] = useState('10');
  const [updoseFreq, setUpdoseFreq] = useState('5');
  const [daysUntilUpdose, setDaysUntilUpdose] = useState('5');

  // const [ITConfig, setITConfig] = useState({
  //   curDosage: 0,
  //   updoseAmt: 10,
  //   updoseFreq: 5,
  //   daysUntilUpdose: 5,
  // });

  return (
    <View style={styles.container}>
      <Text style={styles.title}>{guideName}</Text>
      <Text style={styles.label}>Name of Immunotherapy track:</Text>
      <TextInput
        style={styles.input}
        // keyboardType="number-pad"
        onChangeText={(text) => setITName(text)}
        value={ITName}
      />
      <Text style={styles.label}>Current dosage (in mg):</Text>
      <TextInput
        style={styles.input}
        keyboardType="number-pad"
        onChangeText={(text) => setCurDosage(text)}
        value={curDosage}
      />
      <Text style={styles.label}>Updose amount:</Text>
      <TextInput
        style={styles.input}
        keyboardType="number-pad"
        onChangeText={(text) => setUpdoseAmt(text)}
        value={updoseAmt}
      />
      <Text style={styles.label}>Updose frequency:</Text>
      <TextInput
        style={styles.input}
        keyboardType="number-pad"
        onChangeText={(text) => setUpdoseFreq(text)}
        value={updoseFreq}
      />
      <Text style={styles.label}>Days until next updose:</Text>
      <TextInput
        style={styles.input}
        keyboardType="number-pad"
        onChangeText={(text) => setDaysUntilUpdose(text)}
        value={daysUntilUpdose}
      />
      <CustomButton
        underlayColor="#a6a4a5"
        style={{backgroundColor: '#b6b4b5', margin: 0, marginTop: 20}}
        onPress={() =>
          saveOITData({
            ITName: ITName,
            curDosage: curDosage,
            updoseAmt: updoseAmt,
            updoseFreq: updoseFreq,
            daysUntilUpdose: daysUntilUpdose,
          })
        }>
        <Text>Save information</Text>
      </CustomButton>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    padding: 30,
    // marginBottom: 400,
    backgroundColor: '#d4d6d5',
    color: 'green',
    borderRadius: 10,
  },
  title: {
    fontSize: 23,
    color: '#000000',
  },
  label: {
    marginTop: 4,
    color: '#666666',
    fontSize: 15,
  },
  input: {
    marginTop: 8,
    marginBottom: 8,
    padding: 8,
    backgroundColor: '#c2c1c0',
    borderRadius: 8,
  },
});

export default ITGetStarted;
