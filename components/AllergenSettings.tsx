import React, {useState} from 'react';
import {View, Text, StyleSheet, FlatList} from 'react-native';
import CheckBox from '@react-native-community/checkbox';

const ALL_ALLERGENS: string[] = ['Peanuts', 'Dairy', 'Wheat', 'Fish', 'Sesame', 'Tree Nuts', 'Eggs', 'Gluten', 'Shellfish', 'Soy'];

interface AllergenSettingsItem {
  item: string;
}

interface AllergenSettingsOptionProps {
  allergen: string;
  value: boolean;
  onChange: (allergen: string, isToggled: boolean) => void;
}

const AllergenSettingsOption = ({
  allergen,
  value,
  onChange,
}: AllergenSettingsOptionProps) => {
  return (
    <View style={styles.checkBoxContainer}>
      <CheckBox
        disabled={false}
        value={value}
        onValueChange={(isToggled: boolean) => {
          onChange(allergen, isToggled);
        }}
        style={styles.checkbox}
      />
      <Text style={styles.checkboxText}>{allergen}</Text>
    </View>
  );
};

const AllergenSettings = (props) => {
  const [allergens, setAllergens] = useState([] as string[]);

  return (
    <View style={styles.container}>
      <Text style={styles.title}>Allergens</Text>
      <FlatList
        data={ALL_ALLERGENS}
        renderItem={({item}: AllergenSettingsItem) => (
          <AllergenSettingsOption
            allergen={item}
            value={allergens.includes(item)}
            onChange={(allergen: string, isToggled: boolean) => {
              if (!isToggled) {
                setAllergens(allergens.filter(elem => elem !== allergen));
              } else {
                if (!allergens.includes(allergen)) {
                  setAllergens([...allergens, allergen]);
                }
              }
            }}
          />
        )}
        keyExtractor={item => item}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    padding: 30,
    backgroundColor: '#d4d6d5',
    borderRadius: 10,
    flex: 1,
  },
  title: {
    fontSize: 23,
    color: '#000000',
    marginBottom: 20,
  },
  checkBoxContainer: {
    flexDirection: 'row',
    marginTop: 20,
    marginBottom: 20,
  },
  checkbox: {
    marginLeft: 5,
  },
  checkboxText: {
    marginLeft: 14, marginTop: 3,
  },
});

export default AllergenSettings;
