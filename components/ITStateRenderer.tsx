/*
 * Render an existing config.
 */

import React, {useState, useReducer} from 'react';
import {View, Text, StyleSheet, TextInput} from 'react-native';

import AsyncStorage from '@react-native-async-storage/async-storage';
import CustomButton from './CustomButton';

const ITStateRenderer = ({ITObject}) => {
  // daysUntilUpdose should never be equal to 0, because updose processing is
  // to be done prior to rendering
  console.log('this is the object passed into the render', ITObject);
  return (
    <View style={styles.container}>
      <Text style={styles.title}>{ITObject['ITName']}</Text>

      <View style={{flexDirection: 'row'}}>
        <Text style={styles.label}>{'current dosage of '}</Text>
        <Text style={styles.emphasis}>{ITObject['curDosage']}</Text>
      </View>
      <View style={{flexDirection: 'row'}}>
        <Text style={styles.emphasis}>{ITObject['daysUntilUpdose']}</Text>
        <Text style={styles.label}>{' days until next updose of '}</Text>
        <Text style={styles.emphasis}>{ITObject['updoseAmt']}</Text>
      </View>
      {/* some setter for setting actual dose */}
      <View style={{flexDirection: 'row', marginTop: 20}}>
        <TextInput
          style={styles.input}
          keyboardType="number-pad"
          placeholder="today's dosage"
        />
        <CustomButton
          underlayColor="#a6a4a5"
          style={{
            flex: 4,
            margin: 0,
            height: 40,
            backgroundColor: '#b6b4b5',
          }}>
          <Text>save</Text>
        </CustomButton>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    padding: 30,
    backgroundColor: '#d4d6d5',
    color: 'green',
    borderRadius: 10,
  },
  title: {
    fontSize: 23,
    color: '#000000',
  },
  emphasis: {
    marginTop: 4,
    color: '#666666',
    fontSize: 15,
  },
  label: {
    marginTop: 4,
    color: '#666666',
    fontSize: 15,
  },
  input: {
    padding: 8,
    margin: 0,
    marginRight: 10,
    backgroundColor: '#c2c1c0',
    borderRadius: 8,
    height: 40,
    flex: 10,
  },
});

export default ITStateRenderer;
