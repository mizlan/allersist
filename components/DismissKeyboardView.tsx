import React from 'react';
import {TouchableWithoutFeedback, Keyboard, View} from 'react-native';

// https://stackoverflow.com/questions/29685421/hide-keyboard-in-react-native
const DismissKeyboardHOC = (Comp) => {
  return ({children, ...props}) => (
    <TouchableWithoutFeedback
      onPress={Keyboard.dismiss}
      accessible={false}
      style={{flex: 1}}>
      <Comp {...props}>{children}</Comp>
    </TouchableWithoutFeedback>
  );
};
const DismissKeyboardView = DismissKeyboardHOC(View);

export default DismissKeyboardView;
