import React from 'react';
import {View, Text, StyleSheet, TouchableHighlight} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';

import WebsiteArea from './WebsiteArea';

const trim = (str: string, length = 200) => {
  if (str.length > length) {
    return str.slice(0, length - 3) + '...';
  } else {
    return str;
  }
};

const RecallInfoItem = ({item}) => {
  const title = item.title;
  const link = item.link;
  const description = item.description;
  const date = item.date;
  return (
    <View style={styles.container}>
      <Text style={styles.title}>{title}</Text>
      <Text style={styles.date}>{date}</Text>
      <Text style={styles.para}>{trim(description)}</Text>
      <View
        style={styles.website}>
        <WebsiteArea url={link} description="read more" />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    padding: 15,
    margin: 0,
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
    marginBottom: 12,
  },
  date: {
    color: '#656565',
    marginBottom: 10,
  },
  para: {
    marginBottom: 10,
  },
  website: {
    flex: 1,
    flexDirection: 'row',
    marginBottom: 0,
  },
});

export default RecallInfoItem;
