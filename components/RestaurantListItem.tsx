import React from 'react';
import {View, Text, StyleSheet, TouchableHighlight} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';

import handleOpenURL from '../util/handleOpenURL';
import WebsiteArea from '../components/WebsiteArea';

interface RestaurantListItemProps {
  item: {
    restaurant: {
      description: string;
      address: {
        line1: string;
        city: string;
      };
      website: string | null | undefined;
    };
    sortScore: number;
    distance: number;
  };
}

const RestaurantListItem = ({item}: RestaurantListItemProps) => {
  const restaurant = item.restaurant;
  const address = restaurant.address;
  const url = restaurant.website;
  return (
    <View style={styles.container}>
      <View style={styles.websiteSection}>
        <Text style={styles.restaurantName}>{restaurant.description}</Text>
        <Text>
          {item.sortScore.toFixed(2)}{' '}
          <Ionicons name="star" style={styles.star} size={16} />
        </Text>
      </View>
      <Text>{item.distance.toFixed(2)} mi</Text>
      <Text>
        <Ionicons name="location" style={styles.location} />{' '}
        {address.line1 + ', ' + address.city}
      </Text>
      <View style={styles.websiteSection}>
        <WebsiteArea url={url} description="go to website" />
        <WebsiteArea
          url={`https://www.yelp.com/search?find_desc=${restaurant.description}`}
          description="search on Yelp"
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    padding: 15,
  },
  restaurantName: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  websiteSection: {
    flex: 1,
    flexDirection: 'row',
  },
  restaurantSection: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  star: {
    color: '#e6c42e',
  },
  location: {
    color: '#a4a6a5',
  },
});

export default RestaurantListItem;
