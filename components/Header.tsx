import React from 'react';
import {View, Text, StyleSheet} from 'react-native';

interface HeaderProps {
  title: string;
}

const Header = ({title}: HeaderProps) => {
  return (
    <View style={styles.header}>
      <Text style={styles.text}>{title}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  header: {
    padding: 15,
    paddingTop: 60,
    backgroundColor: '#f3a843',
  },
  text: {
    color: '#101010',
    textAlign: 'center',
    fontSize: 25,
  },
});

export default Header;
