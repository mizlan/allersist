import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableHighlight,
} from 'react-native';

const ITWidget = ({currentDosage, daysUntilUpdose, goToPage}) => {
  return (
    <TouchableHighlight
      style={styles.view}
      onPress={goToPage}
      underlayColor="#e0e0e0">
      <View>
        <Text style={styles.bigText}>{currentDosage}</Text>
        <Text style={styles.widgetText}>today's dosage</Text>
        <Text style={styles.widgetText}>
          <Text style={styles.keyText}>{daysUntilUpdose}</Text> days until next
          updose.
        </Text>
      </View>
    </TouchableHighlight>
  );
};

const styles = StyleSheet.create({
  view: {
    padding: 30,
    backgroundColor: '#d4d6d5',
    color: 'green',
    borderRadius: 10,
  },
  bigText: {
    fontSize: 40,
    textAlign: 'center',
    padding: 10,
    color: '#101010',
  },
  keyText: {
    color: '#101010',
    fontWeight: 'bold',
  },
  widgetText: {
    textAlign: 'center',
    color: '#5c5c5c',
  },
});

export default ITWidget;
