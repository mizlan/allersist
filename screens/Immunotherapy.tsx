// @format

import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  FlatList,
  TextInput,
  TouchableHighlight,
  ActivityIndicator,
  Keyboard,
} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';

import Header from '../components/Header';
import ITGetStarted from '../components/ITGetStarted';
import ITStateRenderer from '../components/ITStateRenderer';
import DismissKeyboardView from '../components/DismissKeyboardView';
import JSONsyntaxHighlight from '../util/jsonSyntax';
import wait from '../util/clogWait';

const IMMUNO_KEY = '@immunotherapy_ITJSON';

/* set flex: 1 to fix FlatList, source:
   https://stackoverflow.com/questions/55298422/react-native-flatlist-not-scrolling-to-end */
const ITDisplay = ({configList}) => {
  /*
   * display the statistics (includes graph).
   */
  return (
    <View style={{flex: 1}}>
      {configList === undefined ? (
        <ActivityIndicator />
      ) : configList.length === 0 ? (
        <View style={{flex: 1}}>
          <DismissKeyboardView
            style={{flex: 1, padding: 15, justifyContent: 'center'}}>
            <ITGetStarted guideName="Get Started." />
          </DismissKeyboardView>
        </View>
      ) : (
        <View style={{flex: 1}}>
          <FlatList
            data={configList}
            renderItem={({item}) => (
              <View style={{flex: 1, marginBottom: 20}}>
                <ITStateRenderer ITObject={item} />
              </View>
            )}
            keyExtractor={(item) => item['ITName'] + item['curDosage']}
            ListFooterComponent={
              <DismissKeyboardView
                style={{flex: 1, marginBottom: 20, justifyContent: 'center'}}>
                <ITGetStarted guideName="Add OIT Track." />
              </DismissKeyboardView>
            }
            style={{padding: 15}}
          />
        </View>
      )}
    </View>
  );
};

const Immunotherapy = () => {
  const [ITInfo, setITInfo] = useState(undefined);
  console.log('rendering Immuno');

  // AsyncStorage.removeItem(IMMUNO_KEY);

  /*
   * Get values from local storage to display.
   */
  useEffect(() => {
    AsyncStorage.getItem(IMMUNO_KEY).then((res) => {
      if (res !== null) {
        setITInfo(JSON.parse(res));
      } else {
        setITInfo([]);
      }
    });
  }, []);

  return (
    <View style={{flex: 1}}>
      <Header title="Immunotherapy" />
      <ITDisplay configList={ITInfo} />
    </View>
  );
};

export default Immunotherapy;
