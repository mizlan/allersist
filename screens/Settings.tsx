import React, {useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  FlatList,
  TextInput,
  TouchableHighlight,
  ActivityIndicator,
} from 'react-native';
import DismissKeyboardView from '../components/DismissKeyboardView';

import Header from '../components/Header';
import AllergenSettings from '../components/AllergenSettings';

const Settings = () => {
  const data = [];
  for (let i = 0; i < 100; i++) {
    data.push(i);
  }
  return (
    <View style={styles.settings}>
      <Header title="Settings" />
      <View style={styles.contents}>
        <AllergenSettings />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  settings: {
    flex: 1,
  },
  contents: {
    flex: 1,
    padding: 20,
  },
});

export default Settings;
