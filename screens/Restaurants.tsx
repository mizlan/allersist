// @format

// 'https://www.allergyeats.com/results/?distance=20&sort=bestmatch'

import React, {useState, useEffect} from 'react';
import {
  View,
  FlatList,
  ActivityIndicator,
  Alert,
} from 'react-native';
import GetLocation from 'react-native-get-location';

import Header from '../components/Header';

import RestaurantListItem from '../components/RestaurantListItem';
import getNearbyRestaurants from '../util/getNearbyRestaurants';

const gloc = async () => {
  console.log('querying location');
  return GetLocation.getCurrentPosition({
    enableHighAccuracy: true,
    timeout: 15000,
  });
};

const Restaurants = () => {
  const [location, setLocation] = useState([null, null]);
  const [restaurantData, setRestaurantData] = useState([]);
  useEffect(() => {
    gloc()
      .then((location) => {
        setLocation(location);
        getNearbyRestaurants(
          location.latitude,
          location.longitude,
          setRestaurantData,
        );
      })
      .catch((error) => {
        const {code, message} = error;
        console.warn(code, message);
        Alert.alert(code + message);
      });
  }, []);
  return location == null || restaurantData == [] ? (
    <View>
      <ActivityIndicator />
    </View>
  ) : (
    <View style={{flex: 1}}>
      <Header title="Restaurants Near You" />
      <FlatList
        data={restaurantData}
        renderItem={({item}) => <RestaurantListItem item={item} />}
        keyExtractor={(item) => item.restaurant._id}
      />
    </View>
  );
};

export default Restaurants;
