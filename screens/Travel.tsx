import React, {useState, useEffect} from 'react';
import {View, Text, FlatList, ActivityIndicator} from 'react-native';

import Header from '../components/Header';

import conglomerateRecalls from '../util/conglomerateRecalls';
import RecallInfoItem from '../components/RecallInfoItem';

const Travel = () => {
  const [recalls, setRecalls] = useState(undefined);
  useEffect(() => {
    conglomerateRecalls().then((res) => setRecalls(res));
  }, []);
  return (
    <View>
      <Header title="Alerts" />
      {recalls === undefined ? (
        <ActivityIndicator />
      ) : (
        <FlatList
          data={recalls}
          renderItem={({item}) => {
            return (
              <View style={{flex: 1}}>
                <RecallInfoItem item={item} />
              </View>
            );
          }}
          keyExtractor={(item) => item.description}
        />
      )}
    </View>
  );
};

export default Travel;
