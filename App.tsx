import React from 'react';
import {View, Text, StyleSheet, Alert} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Ionicons from 'react-native-vector-icons/Ionicons';
// https://www.youtube.com/watch?v=fVoEojORQyQ

import Dashboard from './screens/Dashboard';
import Restaurants from './screens/Restaurants';
import Immunotherapy from './screens/Immunotherapy';
import Travel from './screens/Travel';
import Settings from './screens/Settings';

const Tab = createBottomTabNavigator();

const App = () => {
  return (
    <NavigationContainer>
      {/* https://reactnavigation.org/docs/tab-based-navigation/ */}
      <Tab.Navigator
        screenOptions={({route}) => ({
          tabBarIcon: ({focused, color, size}) => {
            let iconName;
            switch (route.name) {
              case 'Restaurants':
                iconName = focused ? 'fast-food' : 'fast-food-outline';
                break;
              case 'Immunotherapy':
                iconName = focused ? 'bar-chart' : 'bar-chart-outline';
                break;
              case 'Dashboard':
                iconName = focused ? 'home' : 'home-outline';
                break;
              case 'Settings':
                iconName = focused ? 'settings' : 'settings-outline';
                break;
              case 'Travel':
                iconName = focused ? 'airplane' : 'airplane-outline';
                break;
              case 'Alerts':
                iconName = focused ? 'megaphone' : 'megaphone-outline';
                break;
            }
            return <Ionicons name={iconName} size={size} color={color} />;
          },
        })}>
        <Tab.Screen name="Dashboard" component={Dashboard} />
        {/* omit travel for now */}
        <Tab.Screen name="Alerts" component={Travel} />
        <Tab.Screen name="Restaurants" component={Restaurants} />
        <Tab.Screen name="Immunotherapy" component={Immunotherapy} />
        <Tab.Screen name="Settings" component={Settings} />
      </Tab.Navigator>
    </NavigationContainer>
  );
};

export default App;
