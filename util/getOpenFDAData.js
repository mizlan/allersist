import formatDate from './formatDate';

const specDataFormat = (json) => {
  /* saves only the title, id, and published attributes */
  return json.map(
    ({reason_for_recall, product_description, recall_initiation_date}) => ({
      title: reason_for_recall,
      link: null,
      date: formatDate(recall_initiation_date),
      description: product_description,
    }),
  );
};

const getOpenFDAData = async () => {
  let url =
    'https://api.fda.gov/food/enforcement.json?search=reason_for_recall:allergen&sort=report_date:desc&limit=20';
  console.log("fetching openFDA");
  return fetch(url)
    .then((response) => response.json())
    .then((json) => specDataFormat(json.results));
};

export default getOpenFDAData;
