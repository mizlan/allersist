/*
 * format 'YYYYMMDD' to 'YYYY-MM-DD'
 */
function formatDate(dateStr) {
  if (dateStr === undefined) {
    return 'unknown';
  } else {
    return `${dateStr.slice(0, 4)}-${dateStr.slice(4, 6)}-${dateStr.slice(6, 8)}`;
  }
}

export default formatDate;
