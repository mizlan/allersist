import {Alert, Linking} from 'react-native';

const handleOpenURL = async (url) => {
  if (!(url.startsWith('http://') || url.startsWith('https://'))) {
    url = 'http://' + url;
  }
  Linking.openURL(url).catch((reason) => Alert.alert(reason.toString()));
};

export default handleOpenURL;
