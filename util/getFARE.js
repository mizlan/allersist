const specDataFormat = (json) => {
  return json.map(({title, url, summary, date}) => ({
    title,
    link: url,
    date,
    description: summary,
  }));
};

const getFARE = async () => {
  let url = 'https://www.foodallergy.org/api/alerts/results?_limit=10&_page=1';
  console.log("fetching FARE");
  return fetch(url)
    .then((response) => response.json())
    .then((json) => specDataFormat(json.results));
};

export default getFARE;
