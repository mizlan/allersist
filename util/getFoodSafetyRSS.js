import * as rssParser from 'react-native-rss-parser';

const specDataFormat = (rss) => {
  return rss.map(({title, id, published, description}) => ({
    title,
    link: id,
    date: published,
    description,
  }));
};

const getFoodSafetyRSS = async () => {
  let url = 'https://www2c.cdc.gov/podcasts/createrss.asp?c=146';
  console.log("fetching CDC");
  return fetch(url)
    .then((response) => response.text())
    .then((responseData) => rssParser.parse(responseData))
    .then((parsedRss) => specDataFormat(parsedRss.items));
};

export default getFoodSafetyRSS;
