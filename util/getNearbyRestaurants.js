const getNearbyRestaurants = (latitude, longitude, onChange) => {
  const url = `https://api.allergyeats.com/api/v1/restaurant/query?take=20&allergynames=Dairy%2CEggs&distance=20&location=${latitude}%2C${longitude}`;
  fetch(url)
    .then((res) => res.json())
    .then((data) => {
      onChange(data.data.sort((a, b) => (a.sortScore > b.sortScore ? -1 : 1)));
    });
};

export default getNearbyRestaurants;
