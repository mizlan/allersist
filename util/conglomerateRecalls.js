import getFoodSafetyRSS from './getFoodSafetyRSS';
import getOpenFDAData from './getOpenFDAData';
import getFARE from './getFARE';

async function conglomerateRecalls() {
  const recalls = await Promise.all([
    getFoodSafetyRSS(),
    getOpenFDAData(),
    getFARE(),
  ]);
  const fd = recalls.flat();
  const sorted = fd.sort((a, b) => Date.parse(a.date) < Date.parse(b.date));
  // remove duplicates
  const vis = new Set();
  const filtered = [];
  for (let entry of sorted) {
    if (!vis.has(entry.title)) {
      filtered.push(entry);
    }
    vis.add(entry.title);
  }
  return filtered;
}

export default conglomerateRecalls;
