import { Alert } from 'react-native';
import GetLocation from 'react-native-get-location';

const queryLocation = (setter) => {
  console.log('querying location');
  GetLocation.getCurrentPosition({
    enableHighAccuracy: true,
    timeout: 15000,
  })
    .then((location) => {
      setter(location);
    })
    .catch((error) => {
      const {code, message} = error;
      console.warn(code, message);
      Alert.alert(code + message);
    });
};

export default queryLocation;
